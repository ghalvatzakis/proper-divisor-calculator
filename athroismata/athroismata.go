package main

import (
	"bufio"
	"fmt"
	"github.com/tealeg/xlsx"
	"os"
	"strconv"
)

func main() {
	var file *xlsx.File
	var sheet *xlsx.Sheet
	var row *xlsx.Row
	var cell *xlsx.Cell
	var err error

	file = xlsx.NewFile()
	sheet, err = file.AddSheet("Sheet1")
	if err != nil {
		fmt.Printf(err.Error())
	}
	row = sheet.AddRow()
	cell = row.AddCell()

	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Εισάγετε Αριθμό: ")
	text := ""
	for scanner.Scan() {
		text = scanner.Text()
		break
	}

	cell.Value = text

	if s, err := strconv.ParseUint(text, 10, 64); err == nil {
		res := pd(s, file, sheet, row, cell, err)
		// fmt.Println(res);

		for res != 1 {
			res = pd(res, file, sheet, row, cell, err)
		}
    fmt.Print("\nΤέλος Πράξεων.\n")
		bufio.NewReader(os.Stdin).ReadBytes('\n') //system pause
	}

}
func pd(n uint64, file *xlsx.File, sheet *xlsx.Sheet, row *xlsx.Row, cell *xlsx.Cell, err error) uint64 {

	i := uint64(2)
	s := uint64(0)
	for i <= n/2 {
		if n%i == 0 || i == 1 {
			s = s + i
		}
		i++
	}
	if s != 0 {
		fmt.Printf("%s %v\n", "Άθροισμα: ", (s + 1))
		row = sheet.AddRow()
		cell = row.AddCell()
		cell.Value = strconv.FormatUint(s+1, 10)
		
	}
	err = file.Save("MyXLSXFile.xlsx")
	if err != nil {
		fmt.Printf(err.Error())
	}
	return (s + 1)

}
